{-# LANGUAGE DeriveFunctor     #-}
{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeApplications  #-}
module Data.Wakfu.Inspect
    ( BinaryData(..)
    , MemberF(..)
    , MemberTypeF(..)
    , Member
    , MemberType
    , LabelledMember
    , LabelledMemberType
    , extractType
    , inspectBinaryDataStructures
    ) where
import qualified Codec.Archive.Zip    as Zip
import           Control.Foldl        (Fold)
import qualified Control.Foldl        as L
import           Data.Aeson           (FromJSON (..), withObject, (.:))
import           Data.Binary.Get      (runGet)
import qualified Data.ByteString.Lazy as BL
import           Data.Char            (toLower)
import           Data.Fix             (Fix (..), cata)
import           Data.Foldable        (foldl')
import           Data.List            (find)
import           Data.Maybe           (fromJust, maybeToList)
import           Data.Monoid          (All (..), (<>))
import           Data.Set             (Set)
import qualified Data.Set             as Set
import           GHC.Generics         (Generic)
import           Language.JVM.CFG     (bbByPC, bbInsts)
import qualified Language.JVM.Parser  as JVM
import           Path
import           System.FilePath      (takeBaseName, takeExtension)

data BinaryData = BinaryData
    { dataId         :: !Int
    , dataName       :: !String
    , dataMembers    :: ![Member]
    , obfuscatedName :: !String
    } deriving (Eq, Show)

instance Ord BinaryData where
    compare a b = compare (dataId a) (dataId b)

data MemberF a = Member
    { memberName :: String
    , memberType :: MemberTypeF a
    } deriving (Generic, Eq, Show, Functor)

data MemberTypeF a
    = Boolean
    | Int8
    | Int16
    | Int32
    | Int64
    | Float32
    | Float64
    | Timestamp
    | Str
    | Array (MemberTypeF a)
    | HashMap (MemberTypeF a) (MemberTypeF a)
    | Composite [a]
    deriving (Generic, Eq, Show, Functor)

type Member = Fix MemberF
type MemberType = Fix MemberTypeF
type LabelledMember = MemberF Member
type LabelledMemberType = MemberTypeF Member

instance FromJSON (MemberTypeF Member)
instance FromJSON (MemberF Member) where
    parseJSON = withObject "Member" $ \v -> Member
        <$> v .: "name"
        <*> v .: "type"
instance FromJSON Member where
    parseJSON v = Fix <$> parseJSON @(MemberF Member) v

type ClassAccess = (String -> Maybe JVM.Class)

extractType :: Member -> MemberType
extractType m = cata (Fix . memberType) m

inspectBinaryDataStructures :: Path a File -> IO [BinaryData]
inspectBinaryDataStructures path = do
    contents <- BL.readFile $ toFilePath path
    let archive = Zip.toArchive contents
        classEntries = filter ((== ".class") . takeExtension . Zip.eRelativePath) $ Zip.zEntries archive
        classLoader nm = readClass <$> find ((== nm) . stripClassName) classEntries
        foldEntries = flip L.fold classEntries
        bdatas = foldEntries . foldEntries . foldEntries . foldEntries $ collectBinaryData classLoader
    return $ Set.toList bdatas

collectBinaryData :: ClassAccess -> Fold Zip.Entry (Fold Zip.Entry (Fold Zip.Entry (Fold Zip.Entry (Set BinaryData))))
collectBinaryData classLoader =
    fmap (fmap (fmap removeUnreferencedBdataClasses)) $
        fmap collectSubclasses . findBdataEnum <$> L.premap readClass (L.find isBinaryData)
    where
        findBdataEnum :: Maybe JVM.Class -> Fold Zip.Entry ([EnumField], String)
        findBdataEnum (Just base) =
            fmap (\cls -> (getEnumFromBdataClass cls, JVM.className base))
                . L.premap readClass . L.find
                . isBinaryDataSubclass $ JVM.className base
        findBdataEnum Nothing = error "couldn't find the binary data base class"
        getEnumFromBdataClass :: Maybe JVM.Class -> [EnumField]
        getEnumFromBdataClass (Just cls) =
            let Just (storableName, _) = classBinaryDataType cls
            in enumFields . fromJust $ classLoader storableName
        collectSubclasses :: ([EnumField], String) -> Fold Zip.Entry [BinaryData]
        collectSubclasses params =
            L.premap readClass $ L.foldMap (foldSubclasses params) id
        foldSubclasses :: ([EnumField], String) -> JVM.Class -> [BinaryData]
        foldSubclasses (enum, baseName) cls
            | isBinaryDataSubclass baseName cls = maybeToList $ toBinaryData enum cls
            | otherwise = []
        toBinaryData :: [EnumField] -> JVM.Class -> Maybe BinaryData
        toBinaryData enum cls =
            flip fmap (classBinaryDataType cls) $ \(_, fn) ->
                let EnumField nm val _ = enumFromFieldName fn enum
                in BinaryData val (prettyName nm) (defaultMembers $ classBinaryDataMembers classLoader cls) $
                    JVM.className cls
        enumFromFieldName :: String -> [EnumField] -> EnumField
        enumFromFieldName fn = fromJust . find ((== fn) . enumFieldName)
        -- we go through every single class one more time to determine
        -- which binary data classes are ever referenced somewhere in the code,
        -- this has to be done because there are many unused, invalid binary data classes
        removeUnreferencedBdataClasses :: [BinaryData] -> Fold Zip.Entry (Set BinaryData)
        removeUnreferencedBdataClasses bdatas =
            L.premap readClass . flip L.foldMap id $ \cls ->
                let cp = JVM.constantPool cls
                    classRefs = foldl' (collectConstantClasses cp) [] cp
                    used = filter (flip elem classRefs . obfuscatedName) bdatas
                in Set.fromList used
        collectConstantClasses :: JVM.ConstantPool -> [String] -> JVM.ConstantPoolInfo -> [String]
        collectConstantClasses cp ss (JVM.ConstantClass idx) = JVM.poolUtf8 cp idx : ss
        collectConstantClasses _ ss _ = ss
        isBinaryDataSubclass :: String -> JVM.Class -> Bool
        isBinaryDataSubclass baseName cls = elem baseName $ JVM.classInterfaces cls
        -- the binary data interface is recognized by the
        -- types on it's methods
        isBinaryData :: JVM.Class -> Bool
        isBinaryData = getAll . isBinaryData'
            where
                isBinaryData' =
                    (All . JVM.classIsInterface) <>
                    (All . (== 3) . length . JVM.classMethods) <>
                    (All . any (getAll . isReadMethod) . JVM.classMethods) <>
                    (All . any (getAll . isResetMethod) . JVM.classMethods) <>
                    (All . any (getAll . isGetDataTypeId) . JVM.classMethods)
                isReadMethod =
                    (All . (== Nothing) . JVM.methodReturnType) <>
                    (All . (== 1) . length . JVM.methodParameterTypes) <>
                    (All . JVM.isRefType . head . JVM.methodParameterTypes)
                isResetMethod =
                    (All . (== Nothing) . JVM.methodReturnType) <>
                    (All . null. JVM.methodParameterTypes)
                isGetDataTypeId =
                    (All . (== Just JVM.IntType) . JVM.methodReturnType) <>
                    (All . null. JVM.methodParameterTypes)
        prettyName (x:xs) = x:toPascalCase xs
        toPascalCase ('_':x:xs) = x:toPascalCase xs
        toPascalCase (x:xs)     = toLower x:toPascalCase xs
        toPascalCase []         = []

classBinaryDataType :: JVM.Class -> Maybe (String, String)
classBinaryDataType cls =
    case dataTypeIdMethod cls of
        Just m ->
            let (JVM.Code _ _ cfg _ _ _ _) = JVM.methodBody m
                bb = bbByPC cfg 0
            in case fmap (fmap snd . bbInsts) bb of
                (Just (JVM.Getstatic (JVM.FieldId cname fname _) : _)) ->
                    Just (cname, fname)
                _ -> Nothing
        Nothing -> error "Data type id method not found"
    where
        dataTypeIdMethod =
            find (getAll . isDataTypeIdMethod) . JVM.classMethods
        isDataTypeIdMethod =
            (All . JVM.methodIsFinal) <>
            (All . (== Just JVM.IntType) . JVM.methodReturnType) <>
            (All . null . JVM.methodParameterTypes)

classBinaryDataMembers :: ClassAccess -> JVM.Class -> [LabelledMemberType]
classBinaryDataMembers classLoader =
    fmap (fromJVM <$> JVM.fieldType <*> JVM.fieldSignature)
        . filter ((== JVM.Protected) . JVM.fieldVisibility) . JVM.classFields
    where
        fromJVM :: JVM.Type -> Maybe String -> LabelledMemberType
        fromJVM JVM.ShortType _ = Int16
        fromJVM JVM.LongType _ = Int64
        fromJVM JVM.IntType _ = Int32
        fromJVM JVM.FloatType _ = Float32
        fromJVM JVM.DoubleType _ = Float64
        fromJVM JVM.ByteType _ = Int8
        fromJVM JVM.CharType _ = Int8
        fromJVM JVM.BooleanType _ = Boolean
        fromJVM (JVM.ArrayType mt) _ = Array $ fromJVM mt Nothing
        fromJVM (JVM.ClassType "java/lang/String") _ = Str
        fromJVM (JVM.ClassType "java/util/HashMap") (Just sign) =
            -- it's a very naive way to parse type parameterse from
            -- a field signature (e.g. java/util/HashMap<ILjava/lang/String>;)
            let ('<' : kp, ';' : xs) = break (== ';') $ dropWhile (/= '<') sign
                k = kp <> ";"
                v = take (length xs - 2) xs
            in HashMap (fromSignature k) (fromSignature v)
        fromJVM (JVM.ClassType "java/sql/Timestamp") _ = Timestamp
        fromJVM (JVM.ClassType nm) _ =
            Composite . defaultMembers $ classBinaryDataMembers classLoader (fromJust $ classLoader nm)
        fromSignature :: String -> LabelledMemberType
        fromSignature "Ljava/lang/Boolean;" = Boolean
        fromSignature "Z" = Boolean
        fromSignature "Ljava/lang/Long;" = Int64
        fromSignature "J" = Int64
        fromSignature "Ljava/lang/Integer;" = Int32
        fromSignature "I" = Int32
        fromSignature "Ljava/lang/Short;" = Int16
        fromSignature "S" = Int16
        fromSignature "Ljava/lang/Byte;" = Int8
        fromSignature "B" = Int8
        fromSignature "Ljava/lang/Double;" = Float64
        fromSignature "D" = Float64
        fromSignature "Ljava/lang/Float;" = Float32
        fromSignature "F" = Float32
        fromSignature ('[' : xs) = Array $ fromSignature xs
        fromSignature ('L' : xs) =
            let sign = take (length xs - 1) xs
            in fromJVM (JVM.ClassType sign) (Just sign)
        fromSignature _ = error "Unrecognized signature"

data EnumField = EnumField
    { _enumName     :: String
    , _enumValue    :: Int
    , enumFieldName :: String
    } deriving (Show)

enumFields :: JVM.Class -> [EnumField]
enumFields cls =
    case find ((== "<clinit>") . JVM.methodName) $ JVM.classMethods cls of
        Just method ->
            let (JVM.Code _ _ cfg _ _ _ _) = JVM.methodBody method
                code = fmap snd . bbInsts . fromJust $ bbByPC cfg 0
                in parseInitMethod code
        Nothing -> error "No <clinit> method! Invalid class!"
    where
        parseInitMethod :: [JVM.Instruction] -> [EnumField]
        parseInitMethod l =
            case parseEnumInitCode l of
                Just (field, xs) -> field : parseInitMethod xs
                Nothing          -> []
        parseEnumInitCode :: [JVM.Instruction] -> Maybe (EnumField, [JVM.Instruction])
        parseEnumInitCode
            ( JVM.New _
            : JVM.Dup
            : JVM.Ldc (JVM.String ename)
            : JVM.Ldc _
            : JVM.Ldc (JVM.Integer val)
            : JVM.Invokespecial _ _
            : JVM.Putstatic (JVM.FieldId _ fname _)
            : xs) = Just (EnumField ename (fromIntegral val) fname,  xs)
        parseEnumInitCode _ = Nothing


defaultMembers :: [LabelledMemberType] -> [Member]
defaultMembers = zipWith (\i t -> Fix $ Member ('_' : show i) t) [0..]

readClass :: Zip.Entry -> JVM.Class
{-# INLINE readClass #-}
readClass = runGet JVM.getClass . Zip.fromEntry

stripClassName :: Zip.Entry -> String
{-# INLINE stripClassName #-}
stripClassName = takeBaseName . Zip.eRelativePath
