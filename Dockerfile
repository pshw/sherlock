FROM haskell:8.10

WORKDIR /app

COPY . /app

RUN mkdir /app/{game,out} && stack setup && stack build

CMD ["stack", "exec", "sherlock-exe", "/app/game", "/app/out"]
